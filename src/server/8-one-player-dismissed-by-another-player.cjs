const fs = require('fs')
const path = require('path');
const csv = require('csvtojson');
// const matchPath = path.resolve(__dirname, '../data/matches.csv');
const deliveryPath = path.resolve(__dirname, '../data/deliveries.csv');

csv()
.fromFile(deliveryPath)
.then((jsonObj) => {
    const result = dismissedPlayer(jsonObj)
    fs.writeFileSync('../public/output/8-one-player-dismissed-by-another-player.json', JSON.stringify(result));
})


function dismissedPlayer(deliveries){

    let players = {};

    deliveries.map(delivery => {

        if (delivery.player_dismissed){

            if (players[delivery.bowler]){

               if (players[delivery.bowler][delivery.player_dismissed]){
                players[delivery.bowler][delivery.player_dismissed] += 1;
               }

               else{
                players[delivery.bowler][delivery.player_dismissed] = 1;
               }

            } 

            else{
                players[delivery.bowler] = {}
                players[delivery.bowler][delivery.player_dismissed] = 1;
            }
        }
    })

    //console.log(players)

    const sortedPlayers = Object.entries(players).reduce((accumulator,[bowler, dismissedPlayer])=>{ 
        let add =Object.entries(dismissedPlayer).sort(([a1,b1],[a2, b2]) =>  {
            return b2 - b1 
        })[0]
        accumulator[bowler] = {
            "dismissed_player" : add[0],
            "dismissed_count" : add[1]
        } 
        return accumulator;
    }, {})

    console.log(sortedPlayers)

    return sortedPlayers



}