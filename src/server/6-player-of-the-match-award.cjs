const fs = require('fs')
const path = require('path');
const csv = require('csvtojson');
const matchPath = path.resolve(__dirname, '../data/matches.csv');
// const deliveryPath = path.resolve(__dirname, '../data/deliveries.csv');

csv()
.fromFile(matchPath)
.then((jsonObj)=>{

    const result = playerOfTheMatch(jsonObj)
    fs.writeFileSync('../public/output/6-player-of-the-match-award.json', JSON.stringify(result));

})

function playerOfTheMatch(matches){

    const matchesForEachSeason = {};

    matches.map(match => {

        if (matchesForEachSeason[match.season]){

            if (matchesForEachSeason[match.season][match.player_of_match] ){

                matchesForEachSeason[match.season][match.player_of_match]++

            }else {

                matchesForEachSeason[match.season][match.player_of_match] = 1                

            }

        } else {

            matchesForEachSeason[match.season] = {}
            matchesForEachSeason[match.season][match.player_of_match] = 1  

        }
        
    })

    //console.log(matchesForEachSeason)


    const sortedMatchesForEachSeason = Object.entries(matchesForEachSeason).reduce((accumulator, [years, values]) => {
        //console.log(years)
        //console.log(values)

        let s  =  Object.entries(values).sort((a, b) => b[1] - a[1])[0]

        accumulator[years] = {
            "Batsman" : s[0],
            "Awards" : s[1]
        }
        
        return accumulator 
        
    }, {})

   // let result = Object.fromEntries(sortedMatchesForEachSeason)

    return sortedMatchesForEachSeason;

}
