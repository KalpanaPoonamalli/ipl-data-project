const fs = require('fs')
const path = require('path');
const csv = require('csvtojson');
const matchPath = path.resolve(__dirname, '../data/matches.csv');
const deliveryPath = path.resolve(__dirname, '../data/deliveries.csv');


csv()
.fromFile(matchPath)
.then((jsonObj)=>{

    csv()
    .fromFile(deliveryPath)
    .then((Obj) => {
        const result = strikeRateForBatsman(jsonObj, Obj)
        fs.writeFileSync('../public/output/7-strike-rate-of-each-batsman.json', JSON.stringify(result));
    })
})

function strikeRateForBatsman(matches, deliveries){

    const tables = matches.reduce((accumulator, currentValue) => {
        accumulator[currentValue.id] = currentValue.season
        return accumulator
    }, {})

    //console.log(tables)



    //let totalRuns = {};
    //let numberOfBatsman = {};

    const strikeRateForBatsman = {};

    
   deliveries.map(delivery =>{


    let season = tables[delivery.match_id];
   

    if(strikeRateForBatsman[delivery.batsman]){
        if (strikeRateForBatsman[delivery.batsman][season]){
            strikeRateForBatsman[delivery.batsman][season].total_runs += parseInt(delivery.total_runs)
            strikeRateForBatsman[delivery.batsman][season].total_batsman += 1
            strikeRateForBatsman[delivery.batsman][season].strike += parseInt(strikeRateForBatsman[delivery.batsman][season].total_runs/strikeRateForBatsman[delivery.batsman][season].total_batsman)
        }else {
            strikeRateForBatsman[delivery.batsman][season] = {}
            strikeRateForBatsman[delivery.batsman][season].total_runs = parseInt(delivery.total_runs)
            strikeRateForBatsman[delivery.batsman][season].total_batsman = 1
            strikeRateForBatsman[delivery.batsman][season].strike = parseInt(strikeRateForBatsman[delivery.batsman][season].total_runs/strikeRateForBatsman[delivery.batsman][season].total_batsman)
        }
    }else{
        strikeRateForBatsman[delivery.batsman] = {}
        strikeRateForBatsman[delivery.batsman][season] = {}
        strikeRateForBatsman[delivery.batsman][season].total_runs = parseInt(delivery.total_runs)
        strikeRateForBatsman[delivery.batsman][season].total_batsman = 1
        strikeRateForBatsman[delivery.batsman][season].strike = parseInt(strikeRateForBatsman[delivery.batsman][season].total_runs/strikeRateForBatsman[delivery.batsman][season].total_batsman)
    }
        
        
        
        //const strikeRate = {};
        //console.log(totalRuns)
        //console.log(numberOfBatsman)

    }) 

    //const strikeRateForBatsman = {};

    //console.log(numberOfBatsman)
    console.log(strikeRateForBatsman)
    return strikeRateForBatsman;
}
