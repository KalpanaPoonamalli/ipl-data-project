const fs = require('fs')
const path = require('path');
const csv = require('csvtojson');
const matchPath = path.resolve(__dirname, '../data/matches.csv');


csv()
.fromFile(matchPath)
.then((jsonObj)=>{
    //console.log(jsonObj)
    console.log(matchesWonPerTeamPerYear(jsonObj));
    const result = matchesWonPerTeamPerYear(jsonObj)
    fs.writeFileSync('../public/output/2-matches-won-per-team-per-year.json', JSON.stringify(result));
})


function matchesWonPerTeamPerYear(matches){
   
    const matchesWonPerYears = matches.reduce((accumulator, currentValue)=>{
        
        if (accumulator[currentValue.season]){
            if (accumulator[currentValue.season][currentValue.winner]){

                accumulator[currentValue.season][currentValue.winner] ++;
            }else {
                accumulator[currentValue.season][currentValue.winner] = 1;
            }

        } else{

            accumulator[currentValue.season] = {}
            accumulator[currentValue.season][currentValue.winner] = 1;
        }

        return accumulator;
    }, {})


    return matchesWonPerYears
}