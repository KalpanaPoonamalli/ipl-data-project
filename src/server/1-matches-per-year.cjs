const fs = require('fs')
const path = require('path');
const csv = require('csvtojson');
const matchPath = path.resolve(__dirname, '../data/matches.csv');


csv()
.fromFile(matchPath)
.then((jsonObj)=>{
    //console.log(jsonObj)
    //console.log(matchesPerYear(jsonObj));
    const result = matchesPerYear(jsonObj)
    fs.writeFileSync('../public/output/1-matches-per-year.json', JSON.stringify(result));
})


function matchesPerYear(matches){
    if (matches === undefined || !Array.isArray(matches)){
        return {};
    }

    let matchesPlayedPerYear;

    matchesPlayedPerYear = matches.reduce((perYearMatches, match) => {
            perYearMatches[match.season] = (perYearMatches[match.season] || 0) + 1
            return perYearMatches;

    }, {})


    return matchesPlayedPerYear;
}



