const fs = require('fs')
const path = require('path');
const csv = require('csvtojson');
const matchPath = path.resolve(__dirname, '../data/matches.csv');
// const deliveryPath = path.resolve(__dirname, '../data/deliveries.csv');

csv()
.fromFile(matchPath)
.then((jsonObj)=>{

    const result = eachTeamWonTossAndMatch(jsonObj)
    fs.writeFileSync('../public/output/5-each-team-won-toss-and-match.json', JSON.stringify(result));

})

function eachTeamWonTossAndMatch(matches){
    const tossAndMatchWinner = {};

    matches.map(match =>{
        if (match.toss_winner == match.winner){
            tossAndMatchWinner[match.toss_winner] = (tossAndMatchWinner[match.toss_winner] || 0) + 1
        }
    })
    return tossAndMatchWinner;
}