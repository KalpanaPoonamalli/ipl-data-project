const fs = require('fs')
const path = require('path');
const csv = require('csvtojson');
const matchPath = path.resolve(__dirname, '../data/matches.csv');
const deliveryPath = path.resolve(__dirname, '../data/deliveries.csv');

csv()
.fromFile(matchPath)
.then((jsonObj)=>{

    csv()
    .fromFile(deliveryPath)
    .then((Obj) => {
  
        const result = topEconomialBowlers(jsonObj, Obj)
        fs.writeFileSync('../public/output/4-top-economial-bowlers-in-year.json', JSON.stringify(result));
    })
})

function topEconomialBowlers(matches, deliveries){
    const year = 2015;
    const deliveriesDataIn2015 = deliveries.filter(delivery => {
        const match = matches.find(match => match.id === delivery.match_id);
        return match && match.season == year 
    })

    const totalBalls = {};
    const totalRuns = {};

    deliveriesDataIn2015.map(delivery => {
        totalBalls[delivery.bowler] =(totalBalls[delivery.bowler] || 0) + 1
        totalRuns[delivery.bowler] =(totalRuns[delivery.bowler] || 0) + parseInt(delivery.total_runs) 
    })
    //console.log(totalRuns)

    const economyRates = {};

    Object.keys(totalBalls).map((bowler) => {
        const balls = totalBalls[bowler];
        const runs = totalRuns[bowler];
        const economyRate = (runs / balls) * 6;

        economyRates[bowler] = parseInt(economyRate);
    })

    const sortedBowlerEconomy = Object.entries(economyRates).sort((a,b) => a[1] - b[1]);

    return sortedBowlerEconomy.slice(0,10);
}