const fs = require('fs')
const path = require('path');
const csv = require('csvtojson');
//const matchPath = path.resolve(__dirname, '../data/matches.csv');
const deliveryPath = path.resolve(__dirname, '../data/deliveries.csv');

csv()
.fromFile(deliveryPath)
.then((jsonObj)=>{

   const result = bestEconomySuperOver(jsonObj)
    fs.writeFileSync('../public/output/9-bowler-with-best-economy-super-over.json', JSON.stringify(result));
    
})

function bestEconomySuperOver(deliveries){

    const superOvers = {};

    deliveries.map(delivery => {
        superOvers[delivery.bowler] = (superOvers[delivery.bowler] || 0) + parseInt(delivery.is_super_over) 
    })
    //console.log(totalRuns)

    const sortedBowlerEconomy = Object.entries(superOvers).sort((a,b) => b[1] - a[1]);

    console.log(sortedBowlerEconomy);

    console.log(sortedBowlerEconomy[0]);

    return sortedBowlerEconomy.slice(0,1);
}