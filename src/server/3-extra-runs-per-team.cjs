const fs = require('fs')
const path = require('path');
const csv = require('csvtojson');
const matchPath = path.resolve(__dirname, '../data/matches.csv');
const deliveryPath = path.resolve(__dirname, '../data/deliveries.csv');

csv()
.fromFile(matchPath)
.then((jsonObj)=>{

    csv()
    .fromFile(deliveryPath)
    .then((Obj) => {
        //console.log(Obj)

    // console.log(jsonObj)
    // console.log(Obj)
    //console.log(extraRunsPerTeam(jsonObj));
        const result = extraRunsPerTeam(jsonObj, Obj)
        fs.writeFileSync('../public/output/3-extra-runs-per-team.json', JSON.stringify(result));
    })
})


function extraRunsPerTeam( matches, deliveries){
    /* const joinedTable = matches.map(match => ({
        ...match,
        ...deliveries.find(delivery => delivery.match_id === match.id)
    }));

    console.log(joinedTable) */

    const year = 2016;
    const deliveriesDataIn2016 = deliveries.filter(delivery => {
        const match = matches.find(match => match.id === delivery.match_id);
        return match && match.season == year ;
    });

    const extraRunsConceded = {};

    deliveriesDataIn2016.map(delivery => {
        const bowlingTeam = delivery.bowling_team;
        const extraRuns = parseInt(delivery.extra_runs);

        extraRunsConceded[bowlingTeam] = (extraRunsConceded[bowlingTeam] || 0 ) + extraRuns;
    }) 

    return extraRunsConceded;

}

